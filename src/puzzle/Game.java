package puzzle;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.imageio.*;
import javax.swing.*;
import java.util.*;

public class Game extends JComponent{
	final boolean LEFT = false;
	final boolean UP = false;
	final boolean RIGHT = true;
	final boolean DOWN = true;
	final int AIRBLOCK = -1;
	
	final int TARGET_FPS = 60;
	final int WAITTIME = 16;
	final double DEFAULTSPEED = .45;
	final double TESTSPEED = 0.01;
	
	final double BASE_RISE = 0.10;
	final double RISE_INTERVALS = 0.05;
	final int LEVEL_INTERVAL = 200;
	
	final int BLOCKSIZE = 64;
	final int MISC_IMAGES = 15;
	final int CHAR_IMAGES = 4;
	final int ROWLENGTH = 6;
	final int COLUMNHEIGHT = 11;
	final int SWAPPEROFFSET = 9;
	final int TESTOFFSET = 8;
	final int DIDGETS = 10;
	final int COMBOIMAGES = 16;
	final int COMBOIMAGEOFFSET = 4;
	final int CHAINIMAGES = 10;
	
	final int SCORE_ON_CLEAR = 10;
	
	private int frameWidth,
				frameHeight,
				boardX,
				boardY,
				blockNum,
				frameCount,
				character,
				seconds,
				minutes;
	
	private double delta,
				   speed = DEFAULTSPEED,
				   blockSpeed,
				   riseY,
				   riseRate;
	
	private boolean isRunning = true,
					isPaused = false,
					isTesting = false,
					riseButton = false,
					frameAdvance = false;
	
	private int score,
				level;
	private long gameTime;
	
	private Block block;
	private boolean[][] contactGrid = new boolean[ROWLENGTH][COLUMNHEIGHT + 1];
	private boolean[][] spriteGrid = new boolean[ROWLENGTH][COLUMNHEIGHT];
	private boolean[][] nullGrid = new boolean[ROWLENGTH][COLUMNHEIGHT];
	private BufferedImage[] blockImg;
	private BufferedImage[] clearImg;
	private BufferedImage[] darkImg;
	private BufferedImage[] miscImg = new BufferedImage[MISC_IMAGES];
	private BufferedImage[] charImg = new BufferedImage[CHAR_IMAGES];
	private BufferedImage[] numImg = new BufferedImage[10]; //there are 10 numbers!
	private BufferedImage[] comboImg = new BufferedImage[COMBOIMAGES]; //there are 15 different combo images
	private BufferedImage[] chainImg = new BufferedImage[CHAINIMAGES]; //there are 15 different combo images
	
	private Swapper swapper;
	private LossSprite loss;
	private ArrayList<SlideSprite> slideArray = new ArrayList<SlideSprite>();
	private ArrayList<FallSprite> fallArray = new ArrayList<FallSprite>();
	private ArrayList<WaitSprite> waitArray = new ArrayList<WaitSprite>();
	private ArrayList<ClearShape> clearArray = new ArrayList<ClearShape>();
	private ArrayList<ScoreSprite> chainArray = new ArrayList<ScoreSprite>();
	private ArrayList<ScoreSprite> comboArray = new ArrayList<ScoreSprite>();
	/**
	 * @param frameW
	 * @param frameH
	 * @param xPos
	 * @param yPos
	 * @param tCharacter
	 * @param tBlockNum
	 * 
	 * A constructor that sets up the parameters, images, the Block class, and adds a KeyListener.
	 */
	public Game(int frameW, int frameH, int xPos, int yPos, int tCharacter, int tBlockNum){
		frameWidth = frameW;
		frameHeight = frameH;
		blockNum = tBlockNum;
		boardX = xPos;
		boardY = yPos;
		character = tCharacter;
		riseY = 0;
		riseRate = BASE_RISE;
		score = 0;
		level = 1;
		
		block = new Block(blockNum);
		loss = new LossSprite(boardX, boardY + BLOCKSIZE, BLOCKSIZE);
		
		swapper = new Swapper(0, 0);
		
		for(int i = 0; i > blockNum; i++){
			blockImg[i] = null;
		}
		
		try{
			blockImg = new BufferedImage[blockNum];
			clearImg = new BufferedImage[blockNum];
			darkImg = new BufferedImage[blockNum];
			
			blockImg[0] = ImageIO.read(new File("images/blocks/blueblock.png"));
			blockImg[1] = ImageIO.read(new File("images/blocks/greenblock.png"));
			blockImg[2] = ImageIO.read(new File("images/blocks/purpleblock.png"));
			blockImg[3] = ImageIO.read(new File("images/blocks/redblock.png"));
			blockImg[4] = ImageIO.read(new File("images/blocks/yellowblock.png"));
			
			clearImg[0] = ImageIO.read(new File("images/blocks/blueclear.png"));
			clearImg[1] = ImageIO.read(new File("images/blocks/greenclear.png"));
			clearImg[2] = ImageIO.read(new File("images/blocks/purpleclear.png"));
			clearImg[3] = ImageIO.read(new File("images/blocks/redclear.png"));
			clearImg[4] = ImageIO.read(new File("images/blocks/yellowclear.png"));
			
			darkImg[0] = ImageIO.read(new File("images/blocks/bluedark.png"));
			darkImg[1] = ImageIO.read(new File("images/blocks/greendark.png"));
			darkImg[2] = ImageIO.read(new File("images/blocks/purpledark.png"));
			darkImg[3] = ImageIO.read(new File("images/blocks/reddark.png"));
			darkImg[4] = ImageIO.read(new File("images/blocks/yellowdark.png"));
			
			if(blockNum == 6){
				blockImg[5] = ImageIO.read(new File("images/blocks/darkblueblock.png"));
				clearImg[5] = ImageIO.read(new File("images/blocks/darkblueclear.png"));
				darkImg[5] = ImageIO.read(new File("images/blocks/darkbluedark.png"));
			}
			
			miscImg[0] = ImageIO.read(new File("images/misc/bigswapper.png"));
			miscImg[1] = ImageIO.read(new File("images/misc/smallswapper.png"));
			miscImg[2] = ImageIO.read(new File("images/misc/frame1.png"));
			miscImg[3] = ImageIO.read(new File("images/misc/frame2.png"));
			miscImg[4] = ImageIO.read(new File("images/misc/background.png"));
			miscImg[5] = ImageIO.read(new File("images/misc/test.png"));
			miscImg[6] = ImageIO.read(new File("images/misc/test2.png"));
			miscImg[7] = ImageIO.read(new File("images/misc/test3.png"));
			miscImg[8] = ImageIO.read(new File("images/misc/test4.png"));
			miscImg[9] = ImageIO.read(new File("images/misc/background2.png"));
			miscImg[10] = ImageIO.read(new File("images/misc/panel.png"));
			miscImg[11] = ImageIO.read(new File("images/misc/score.png"));
			miscImg[12] = ImageIO.read(new File("images/misc/speedlv.png"));
			miscImg[13] = ImageIO.read(new File("images/misc/lose.png"));
			miscImg[14] = ImageIO.read(new File("images/numbers/colon.png"));
			
			charImg[0] = ImageIO.read(new File("images/chars/yoshi.png"));
			charImg[1] = ImageIO.read(new File("images/chars/lakitu.png"));
			charImg[2] = ImageIO.read(new File("images/chars/bumpty.png"));
			charImg[3] = ImageIO.read(new File("images/chars/raven.png"));
			
			for(int i = 0; i < DIDGETS; i++){
				numImg[i] = ImageIO.read(new File("images/numbers/" + i + ".png"));
			}
			
			for(int i = 0; i < CHAINIMAGES; i++){
				chainImg[i] = ImageIO.read(new File("images/chains/" + i + "chain.png"));
			}
			
			for(int i = 0; i < COMBOIMAGES; i++){
				int num = i + COMBOIMAGEOFFSET;
				comboImg[i] = ImageIO.read(new File("images/combos/" + num  + "combo.png"));
			}
		} 
		catch(IOException e)
		{
			System.out.println("One or more image files could not be found");
		}
		
		KeyListener listener = new MyKeyListener();
		addKeyListener(listener);
		setFocusable(true); 
	}
	
	
	/**
	 * @author Jacob
	 * Assigns keys to move the swapper, to swap blocks and to rise.
	 */
	public class MyKeyListener implements KeyListener{
		public void keyTyped(KeyEvent e){}
		public void keyPressed(KeyEvent e){
			switch(e.getKeyChar()){
				case 'w':
				case 'W':swapper.moveUp(); break;
				case 'a':
				case 'A':swapper.moveLeft(); break;
				case 's':
				case 'S':swapper.moveDown(); break;
				case 'd':
				case 'D':swapper.moveRight(); break;
				case 'q':
				case 'Q':if(isTesting){
							isTesting = false;
						}else{
							isTesting = true;
						}break;
				case 'e':
				case 'E':if(speed == DEFAULTSPEED){
							speed = TESTSPEED;
						}else{
							speed = DEFAULTSPEED;
						}break;
//				case 'f':if(isPaused){
//						 	isPaused = false;
//						 	System.out.println("set false");
//						 }else{
//							 isPaused = true;
//							 System.out.println("set true");
//						 }break;
				case 'g': frameAdvance = true; break;
				case '1':swap(); break;
				case '3':riseButton = true; break;
			}
		}
		public void keyReleased(KeyEvent e){
		}
	}
	
	/**
	 * Finds the swapper's positions, and then swaps two blocks if they're not on the nullGrid and not both air.
	 * There are three cases, one if both blocks are not air blocks, and two if there is one that depends on the air blocks position.
	 * Once the case is found the blocks are put on the nullGrid and swapped, then they are put onto the SlideArray as slideSprites
	 */
	public void swap(){
		int leftX = swapper.getX();
		int rightX = swapper.getX() + 1;
		int y = swapper.getY();
				
		if((contactGrid[leftX][y] || contactGrid[rightX][y]) && //only swap if there is at least one contact block
		  (!nullGrid[leftX][y] && !nullGrid[rightX][y])){ // and both blocks are not null
			
			//if both are two contact blocks
			if(contactGrid[leftX][y] && contactGrid[rightX][y]){
				slideArray.add(new SlideSprite(RIGHT, 0, block.getType(leftX, y), leftX, y, 0));
				slideArray.add(new SlideSprite(LEFT, 0, block.getType(rightX, y), rightX, y, 0));
				int temp = block.getType(leftX, y);
				block.setType(leftX, y, block.getType(rightX, y));
				block.setType(rightX, y, temp);
			}
			//if there is just one contact block
			else{
				//if that contact block is the one on the right
				if(contactGrid[rightX][y]){
					slideArray.add(new SlideSprite(LEFT, 0, block.getType(rightX, y), rightX, y, 1));
					block.setType(leftX, y, block.getType(rightX, y));
					block.setType(rightX, y, AIRBLOCK);
				}
				//if that contact block is the one on the left
				else{
					slideArray.add(new SlideSprite(RIGHT, 0, block.getType(leftX, y), leftX, y, 1));
					block.setType(rightX, y, block.getType(leftX, y));
					block.setType(leftX, y, AIRBLOCK);
					
				}
			}
		}
	}
	
	/**
	 *Updates each sliding block's position. If it has reached it destination check if its in a position to fall, if so convert it into a waitSprite
	 *and put it in the waitArray. Finally remove the slideSprite from the SlideArray.
	 */
	public void slideBlocks(){
		boolean[] removedNodes = new boolean[slideArray.size()];
		
		//for every sprtie in the attay
		for(int i = 0; i < slideArray.size(); i++){
			//if the sprite is going left
			if(slideArray.get(i).getDirection() == LEFT){
				double temp = slideArray.get(i).getX() - blockSpeed;
				//if temp is larger than where the sprite is going
				if(temp < slideArray.get(i).getGoalX()){
					slideArray.get(i).setX(slideArray.get(i).getGoalX());
					removedNodes[i] = true;
				}else
				{
					slideArray.get(i).setX(temp);
					//System.out.println("Sprite [" + i + "]: " + slideArray.get(i).toString());
				}
				
			//if the sprite is going right	
			}else{
				double temp = slideArray.get(i).getX() + blockSpeed;
				//if temp is larger than where the sprite is going
				if(temp > slideArray.get(i).getGoalX()){
					slideArray.get(i).setX(slideArray.get(i).getGoalX());
						removedNodes[i] = true;
				}else
				{
					slideArray.get(i).setX(temp);
				}
			}
		}
		
		for(int i = slideArray.size() - 1; i > -1; i--){
			if(removedNodes[i]){
				//if its already in a state to fall
				if(slideArray.get(i).getGoalX()  != COLUMNHEIGHT && slideArray.get(i).getGoalY() != COLUMNHEIGHT - 1 &&!contactGrid[slideArray.get(i).getGoalX()][slideArray.get(i).getGoalY() + 1]){
					waitArray.add(new WaitSprite(slideArray.get(i).getType(), slideArray.get(i).getGoalX(), (int) slideArray.get(i).getY(), WAITTIME, 1));
					block.setType(slideArray.get(i).getGoalX(), slideArray.get(i).getGoalY(), AIRBLOCK);
				}
				if(slideArray.get(i).getY() != 0 && slideArray.get(i).getAir() == 1 && !contactGrid[(int) slideArray.get(i).getGoalX()][(int) slideArray.get(i).getY() - 1]){
					if(slideArray.get(i).getDirection() == LEFT){
						if(contactGrid[(int) slideArray.get(i).getGoalX() + 1][(int) slideArray.get(i).getY() - 1]){
							waitArray.add(new WaitSprite(AIRBLOCK, slideArray.get(i).getGoalX() + 1, (int) slideArray.get(i).getY(), WAITTIME, 1));	
						}
					}else{
						if(contactGrid[(int) slideArray.get(i).getGoalX() - 1][(int) slideArray.get(i).getY() - 1]){
							waitArray.add(new WaitSprite(AIRBLOCK, slideArray.get(i).getGoalX() - 1, (int) slideArray.get(i).getY(), WAITTIME, 1));
						}
					}
				}
				slideArray.remove(i);
			}
		}
	}
	

	/**
	 * Search through block left to right, second to the bottom to  the top and find if the block below each block is an airblock and not null
	 * If that is the case add it to fallArray and set it's position to be an airblock and make it not a contact block.
	 */
	public void gravity(){
		for(int i = 0; i < ROWLENGTH; i++){
			for(int k = COLUMNHEIGHT - 2; k > - 1; k--){ //search from bottom to top starting from the second most bottom block
				if(!contactGrid[i][k + 1] && block.getType(i, k) != AIRBLOCK && !nullGrid[i][k]){
					fallArray.add(new FallSprite(block.getChain(i, k), block.getType(i, k), i, k));
					block.setType(i, k, AIRBLOCK);
					contactGrid[i][k] = false;
				}
			}
		}
	}
	
	/**
	 * Update each FallSprite's position in the fallArray. If it touches a contact block set it to be one block above the contact block and remove it from the FallArray.
	 */
	public void fall(){
		boolean[] removed = new boolean[fallArray.size()];
		
		for(int i = 0; i < fallArray.size() ; i++){
			double temp = fallArray.get(i).getY() + blockSpeed;
			int nextContactBlock = COLUMNHEIGHT;
			
			//find next contact block
			for(int k = (int) COLUMNHEIGHT - 1; k > temp; k--){
				if(contactGrid[fallArray.get(i).getX()][k]){
					nextContactBlock = k;
				}
			}
		
			if(temp >= nextContactBlock - 1){ //if temp has reached it's destination
				block.setType(fallArray.get(i).getX(), nextContactBlock - 1, fallArray.get(i).getType());
				block.setChain(fallArray.get(i).getX(), nextContactBlock - 1, fallArray.get(i).getChain());
				nullGrid[(int) fallArray.get(i).getX()][(int) fallArray.get(i).getY() + 1] = false;
				contactGrid[fallArray.get(i).getX()][(int) temp] = true;
				
				removed[i] = true;
			}
			else{
				fallArray.get(i).setY(temp);
			}	
		}
		for(int i = fallArray.size() - 1; i > -1; i--){
			if(removed[i]){
				fallArray.remove(i);
			}
		}
	}
	
	public void paintComponent(Graphics g){
		Graphics2D g2 = (Graphics2D) g;
		
		final int FRAMEOFFSET = 16;
		final int PANELOFFSET = boardX + FRAMEOFFSET + (BLOCKSIZE * ROWLENGTH);
		final int RIGHT_JUSTIFY_OFFSET = 245;
		final int LEFT_JUSTIFY_OFFSET = 16;
		final int SCORE_X = PANELOFFSET + RIGHT_JUSTIFY_OFFSET;
		final int ITEMOFFSET = 52;
		final int COMBO_OFFSET = 64;
		
		//Draw frames and backgrounds
		g2.drawImage(miscImg[9], 0, 0, null);
		g2.drawImage(miscImg[4], boardX - FRAMEOFFSET, boardY - FRAMEOFFSET, null);
		g2.drawImage(miscImg[10], PANELOFFSET, boardY - FRAMEOFFSET, null);
		g2.drawImage(charImg[character], boardX, boardY, null);
		
		//Draw thigns to the panel
		g2.drawImage(miscImg[11], PANELOFFSET + LEFT_JUSTIFY_OFFSET, boardY + ITEMOFFSET, null);     //Score
		drawNum(g2, score, SCORE_X, boardY + ITEMOFFSET * 2);									     //     1000
		g2.drawImage(miscImg[12], PANELOFFSET + LEFT_JUSTIFY_OFFSET, boardY + ITEMOFFSET * 3, null); //Speed Lv.
		drawNum(g2, level, SCORE_X, boardY + ITEMOFFSET * 4);									     //        1
		drawTimer(g2, SCORE_X, boardY + ITEMOFFSET * 6);									     //    3  20
		
		//Draw blocks on grid
		for(int i = 0; i < ROWLENGTH; i++){
			for(int k = 0; k < COLUMNHEIGHT; k++)
				if(block.getType(i, k) != AIRBLOCK && !spriteGrid[i][k])
					g2.drawImage(blockImg[block.getType(i, k)], boardX + i * BLOCKSIZE, (int) riseY + boardY + (k + 1) * BLOCKSIZE, null);
		}
		//draw ghost row
		int[] ghost = block.getGhostArray();
		for(int i = 0; i < ROWLENGTH; i++){
			g2.drawImage(darkImg[ghost[i]], boardX + i * BLOCKSIZE, boardY + (int) riseY + ((COLUMNHEIGHT + 1) * BLOCKSIZE), null);
		}
		//draw clearing blocks
		for(int i = 0; i < clearArray.size(); i++){
			for(int j = 0; j < clearArray.get(i).getNumOfBlocks(); j++){
				if(clearArray.get(i).getSprite(j).getStatus() && clearArray.get(i).getSprite(i).getType() != AIRBLOCK){
					g2.drawImage(clearImg[clearArray.get(i).getSprite(j).getType()]
								 ,boardX + clearArray.get(i).getSprite(j).getX() * BLOCKSIZE, (int) riseY + boardY + (clearArray.get(i).getSprite(j).getY() + 1) * BLOCKSIZE, null);
				}
			}
		}
		
		//Draw sliding sprites
		for(int i = 0; i < slideArray.size(); i++){
			if(slideArray.get(i).getType() != AIRBLOCK){
				g2.drawImage(blockImg[slideArray.get(i).getType()], boardX + (int) (slideArray.get(i).getX() * BLOCKSIZE), (int) riseY + boardY + (int)((slideArray.get(i).getY() + 1) * BLOCKSIZE), null);
			}
		}
		//Draw waiting blocks that are not air
		for(int i = 0; i < waitArray.size(); i++){
			if(waitArray.get(i).getType() != AIRBLOCK){
				g2.drawImage(blockImg[waitArray.get(i).getType()], boardX + (int) (waitArray.get(i).getX() * BLOCKSIZE), (int) riseY + boardY + (int)((waitArray.get(i).getY() + 1) * BLOCKSIZE), null);
			}
		}
		//Draw falling sprites
		for(int i = 0; i < fallArray.size(); i++){
			if(fallArray.get(i).getType() != AIRBLOCK){
				g2.drawImage(blockImg[fallArray.get(i).getType()], boardX + (int) (fallArray.get(i).getX() * BLOCKSIZE), (int) riseY + boardY + (int)((fallArray.get(i).getY() + 1) * BLOCKSIZE), null);
			}
		}
		//Draw and animate swapper
		if((frameCount % TARGET_FPS * 2) > TARGET_FPS){
			g2.drawImage(miscImg[0], (swapper.getX() * BLOCKSIZE) + boardX - SWAPPEROFFSET, (swapper.getY() * BLOCKSIZE) + (int) riseY + boardY - SWAPPEROFFSET + BLOCKSIZE, null);
		}
		else{
			g2.drawImage(miscImg[1], (swapper.getX() * BLOCKSIZE) + boardX - SWAPPEROFFSET, (swapper.getY() * BLOCKSIZE) + (int) riseY + boardY - SWAPPEROFFSET + BLOCKSIZE, null);
		}
		
		//Draw Frame
		g2.drawImage(miscImg[2], boardX - FRAMEOFFSET, boardY - FRAMEOFFSET, null);
		
		//Draw combo and chain ScoreSprites
		for(int i = 0; i < chainArray.size(); i++){
			g2.drawImage(chainImg[chainArray.get(i).getNum()], boardX + chainArray.get(i).getX(), boardY + (int) chainArray.get(i).getY(), null);
		}
		for(int i = 0; i < comboArray.size(); i++){
			g2.drawImage(comboImg[comboArray.get(i).getNum()], boardX + comboArray.get(i).getX(), boardY + (int) comboArray.get(i).getY() + COMBO_OFFSET, null);
		}
		
		if(isTesting){	
			//draw test images
			for(int i = 0; i < ROWLENGTH; i++){
				for(int k = 0; k < COLUMNHEIGHT; k++){
					if(contactGrid[i][k] == true){
						g2.drawImage(miscImg[5], boardX + i * BLOCKSIZE, (int) riseY + boardY + (k + 1) * BLOCKSIZE, null);
					}
					if(nullGrid[i][k]){
						g2.drawImage(miscImg[6], boardX + TESTOFFSET + i * BLOCKSIZE, (int) riseY + boardY + (k + 1) * BLOCKSIZE, null);
					}
					if(spriteGrid[i][k]){
						g2.drawImage(miscImg[7], boardX + (TESTOFFSET  * 3) + i * BLOCKSIZE, (int) riseY + boardY + (k + 1) * BLOCKSIZE, null);
					}
				}
			}
		}
		
		if(!isRunning){
			drawLoss(g2);
		}
	}
	
	public void drawNum(Graphics2D g2, int num, int xPos, int yPos){
		final int CHAR_OFFSET = 48;
		final int NUM_SIZE = 36;
		char[] numArray;
		numArray = Integer.toString(num).toCharArray();
		int length = Integer.toString(num).length();
		for(int i = 0; i < length; i++){
			g2.drawImage(numImg[numArray[i] - CHAR_OFFSET], xPos - ((length - i) * NUM_SIZE), yPos, null);
		}
	}
	
	public void drawTimer(Graphics2D g2, int xPos, int yPos){
		final int NUM_SIZE = 36;
		final int COLONOFFSET = 108;
		if((frameCount / TARGET_FPS) > (minutes * 60) + seconds){
			seconds++;
		}
		if(seconds > 59){
			seconds = 0;
			minutes++;
		}
		
		if(seconds < 10){
			g2.drawImage(numImg[seconds], xPos - NUM_SIZE, yPos, null);
			g2.drawImage(numImg[0], xPos - (2 * NUM_SIZE), yPos, null);
		}else{
			g2.drawImage(numImg[seconds % 10], xPos - NUM_SIZE, yPos, null);
			g2.drawImage(numImg[seconds / 10], xPos - (2 * NUM_SIZE), yPos, null);
		}
		g2.drawImage(miscImg[14], xPos - COLONOFFSET, yPos, null);
		drawNum(g2, minutes, xPos - (3 * NUM_SIZE), yPos);
		
	}
	
	public void drawLoss(Graphics2D g2){
		loss.update();
		g2.drawImage(miscImg[13], loss.getX(), loss.getY(), null);
		
	}
	
	public void gameLoop(){
		long lastLoopTime = System.nanoTime();
	    final long OPTIMAL_TIME = 1000000000 / TARGET_FPS;
	    long lastFpsTime = 0;
		
		while(isRunning){
			if(!isPaused){
				long now = System.nanoTime();
				long updateLength = now - lastLoopTime;
				lastLoopTime = now;
				delta = updateLength / ((double) OPTIMAL_TIME);
				blockSpeed = delta * speed;
				
				lastFpsTime += updateLength;
				if(lastFpsTime >= 1000000000){
					lastFpsTime = 0;
				}
				
				updateGame();
				paintImmediately(0, 0, frameWidth, frameHeight);
				frameCount++;
				
				try{
					gameTime = (lastLoopTime 
							- System.nanoTime() + OPTIMAL_TIME) / 1000000;
					Thread.sleep(gameTime);
				}catch(Exception e){
				}
				
				if(frameAdvance){
					frameAdvance = false;
				}
			}
		}
	}
	
	public void updateGame(){
		resetChain();
		updateClear();
		slideBlocks();
		updateComboChain();
		updateWaitArray();
		updateSprites();
		updateContact();
		updateNull();
		gravity();
		fall();
		checkMatch();
		rise();
	}
	
	public void resetChain(){
		for(int i = 0; i < ROWLENGTH; i++){
			for(int k = 0; k < COLUMNHEIGHT; k++){
				block.setChain(i, k, 0);
			}
		}
	}
	
	public void updateComboChain(){
		for(int i = comboArray.size() - 1; i > -1; i--){
			if(comboArray.get(i).Update(delta)){
				comboArray.remove(i);
			}
		}
		
		for(int i = chainArray.size() - 1; i > -1; i--){
			if(chainArray.get(i).Update(delta)){
				chainArray.remove(i);
			}
		}
	}
	
	public void updateWaitArray(){
		if(waitArray.size() != 0){
			boolean[] removedNodes = new boolean[waitArray.size()];
			
			for(int i = 0; i < waitArray.size(); i++){
				if(waitArray.get(i).getWaitTime() == 0){
					removedNodes[i] = true;
				}else{
					waitArray.get(i).decreaseTime(delta);
					spriteGrid[waitArray.get(i).getX()][waitArray.get(i).getY()] = true;
				}
			}
			
			for(int i = waitArray.size() - 1; i > -1; i--){
				if(removedNodes[i]){
					if(waitArray.get(i).getType() != AIRBLOCK && !contactGrid[waitArray.get(i).getX()][waitArray.get(i).getY() + 1]){
						fallArray.add(new FallSprite(waitArray.get(i).getChain(), waitArray.get(i).getType(), waitArray.get(i).getX(), waitArray.get(i).getY()));
					}
					for(int k = 0; k < waitArray.get(i).getY(); k++){
						block.setChain(waitArray.get(i).getX(), k, waitArray.get(i).getChain());
					}
					waitArray.remove(i);
				}
			}
		}
	}
	
	public void updateSprites(){
		for(int i = 0; i < ROWLENGTH; i++){
			for(int  k = 0; k < COLUMNHEIGHT; k++){
				spriteGrid[i][k] = false;
			}
		}
		
		for(int i = 0; i < fallArray.size(); i++){
			double temp = fallArray.get(i).getY() + delta;
			blockSpeed = delta * speed;
			int nextContactBlock = COLUMNHEIGHT;
			
			//find next contact block
			for(int k = (int) COLUMNHEIGHT - 1; k > temp; k--){
				if(contactGrid[fallArray.get(i).getX()][k]){
					nextContactBlock = k;
				}
			}
			if(temp < nextContactBlock - 2){ //if temp isn't one contact block above its destination
				spriteGrid[(int) fallArray.get(i).getX()][(int) fallArray.get(i).getY()] = true;
			}
		}
		
		for(int i = 0; i < slideArray.size(); i++){
			spriteGrid[(int) slideArray.get(i).getX()][(int) slideArray.get(i).getY()] = true;
			spriteGrid[(int) slideArray.get(i).getX() + 1][(int) slideArray.get(i).getY()] = true;
		}
		
		for(int i = 0; i < clearArray.size(); i++){
			ArrayList<Sprite> sprite = clearArray.get(i).getArrayList();
			for(int j = 0; j < sprite.size(); j++){
				spriteGrid[sprite.get(j).getX()][sprite.get(j).getY()] = true;
			}
		}
	}
	
	public void updateContact(){
		for(int i = 0; i < ROWLENGTH; i++){
			for(int  k = 0; k < COLUMNHEIGHT; k++){
				contactGrid[i][k] = false;
			}
			contactGrid[i][COLUMNHEIGHT] = true;
		}
		for(int i = 0; i < slideArray.size(); i++){
			contactGrid[(int) slideArray.get(i).getX()][(int) slideArray.get(i).getY()] = true;
			contactGrid[(int) slideArray.get(i).getX() + 1][(int) slideArray.get(i).getY()] = true;
		}
		for(int i = 0; i < waitArray.size(); i++){
			contactGrid[waitArray.get(i).getX()][waitArray.get(i).getY()] = true;
		}
			
		for(int i = 0; i < ROWLENGTH; i++){
			for(int  k = 0; k < COLUMNHEIGHT; k++){
				if(block.getType(i, k) != AIRBLOCK){
					contactGrid[i][k] = true;
				}
			}
		}
	}
	
	public void updateNull(){
		for(int i = 0; i < ROWLENGTH; i++){
			for(int  k = 0; k < COLUMNHEIGHT; k++){
				nullGrid[i][k] = false;
			}
		}
		
		for(int i = 0; i < waitArray.size(); i++){
			nullGrid[waitArray.get(i).getX()][waitArray.get(i).getY()] = true;
			if(!contactGrid[waitArray.get(i).getX()][waitArray.get(i).getY() + 1])
				nullGrid[waitArray.get(i).getX()][waitArray.get(i).getY() + 1] = true;
			
			for(int k = 0; k < waitArray.get(i).getY(); k++){
				nullGrid[waitArray.get(i).getX()][k] = true;
			}
		}
		
		for(int i = 0; i < slideArray.size(); i++){
			if(slideArray.get(i).getDirection() == LEFT){
				nullGrid[(int) slideArray.get(i).getX() + 1][(int) slideArray.get(i).getY()] = true;
			}

			if(!contactGrid[(int)slideArray.get(i).getX()][(int)slideArray.get(i).getY() + 1]){
				nullGrid[(int)slideArray.get(i).getX()][(int)slideArray.get(i).getY() + 1] = true;
			}
			else{
				nullGrid[(int) slideArray.get(i).getX()][(int) slideArray.get(i).getY()] = true;
				if(!contactGrid[(int)slideArray.get(i).getX() + 1][(int)slideArray.get(i).getY() + 1]){
					nullGrid[(int)slideArray.get(i).getX() + 1][(int)slideArray.get(i).getY() + 1] = true;
				}
			}
	
			nullGrid[(int) slideArray.get(i).getGoalX()][(int) slideArray.get(i).getY()] = true;
		}
		
		for(int i = 0; i < fallArray.size(); i++){
			nullGrid[(int) fallArray.get(i).getX()][(int) fallArray.get(i).getY()] = true;
			if(fallArray.get(i).getY() != COLUMNHEIGHT - 1){
				nullGrid[(int) fallArray.get(i).getX()][(int) fallArray.get(i).getY() + 1] = true;
			}
		}
		
		for(int i = 0; i < clearArray.size(); i++){
			for(int j = 0; j < clearArray.get(i).getNumOfBlocks(); j++){
				nullGrid[clearArray.get(i).getSprite(j).getX()][clearArray.get(i).getSprite(j).getY()] = true;
			}
		}
	}
	
	public void checkMatch(){
		int currentType = -2;
		int lineCount = 1;
		int[][] shape = new int[ROWLENGTH][COLUMNHEIGHT];
		boolean hasMatched = false;
		//vertical check
		
		for(int i = 0; i < ROWLENGTH; i++){
			for(int k = 0; k < COLUMNHEIGHT; k++){
				shape[i][k] = AIRBLOCK;
				if(block.getType(i, k) == currentType && block.getType(i, k) != AIRBLOCK && !nullGrid[i][k]){
					lineCount++;
					if(lineCount > 3){
						shape[i][k] = block.getType(i, k);
					}
					else if(lineCount == 3){
						hasMatched = true;
						shape[i][k] = block.getType(i, k);
						shape[i][k - 1] = block.getType(i, k - 1);
						shape[i][k - 2] = block.getType(i, k - 2);
					}
				}
				else{
					if(!nullGrid[i][k]){
						currentType = block.getType(i, k);
					}else{
						currentType = -2;
					}
					lineCount = 1;
				}
			}
			currentType = -2;
		}
//		//horizontal check
		for(int k = 0; k < COLUMNHEIGHT; k++){
			for(int i = 0; i < ROWLENGTH; i++){
				if(block.getType(i, k) == currentType && block.getType(i, k) != AIRBLOCK  && !nullGrid[i][k]){
					lineCount++;
					if(lineCount > 3){
						shape[i][k] = block.getType(i, k);
					}
					else if(lineCount == 3){
						hasMatched = true;
						shape[i][k] = block.getType(i, k);
						shape[i - 1][k] = block.getType(i - 1, k);
						shape[i - 2][k] = block.getType(i - 2, k);
					}
				}
				else{
					if(!nullGrid[i][k]){
						currentType = block.getType(i, k);
					}else{
						currentType = -2;
					}
					lineCount = 1;
				}
			}
			currentType = -2;
		}
		if(hasMatched){
			int highestChain = 1;
			for(int k = 0; k < COLUMNHEIGHT; k++){
				for(int i = 0; i < ROWLENGTH; i++){
					if(shape[i][k] != AIRBLOCK){
						if(block.getChain(i, k) > highestChain){
							highestChain = block.getChain(i, k);
						}
					}
				}
			}
			clearArray.add(new ClearShape(shape, highestChain));
			
			int newIndex = clearArray.size() - 1;
			
			if(clearArray.get(newIndex).getChain() > 1){
				chainArray.add(new ScoreSprite(clearArray.get(newIndex).getXStart(), clearArray.get(newIndex).getYStart(), clearArray.get(newIndex).getChain()));
			}
			
			if(clearArray.get(newIndex).getTotal() > COMBOIMAGEOFFSET - 1){
				comboArray.add(new ScoreSprite(clearArray.get(newIndex).getXStart(), clearArray.get(newIndex).getYStart(), clearArray.get(newIndex).getTotal() - COMBOIMAGEOFFSET));
			}
		}
		
	}
	
	/**
	 * Updates Each
	 */
	public void updateClear(){
		for(int i = clearArray.size() - 1; i > -1; i--){
			if(clearArray.get(i).update(delta)){
				ArrayList<Sprite> sprite = clearArray.get(i).getArrayList();
				int[] top = new int[ROWLENGTH];
				for(int j = 0; j < ROWLENGTH; j++){
					top[j] = -1;
				}
				for(int j = sprite.size() - 1; j > -1; j--){
					block.setType(sprite.get(j).getX(), sprite.get(j).getY(), AIRBLOCK);
					nullGrid[sprite.get(j).getX()][sprite.get(j).getY()] = false;
					top[sprite.get(j).getX()] = sprite.get(j).getY();
				}
				
				for(int j = 0; j < ROWLENGTH; j++){
					if(top[j] != -1){
						for(int k = 0; k < top[j]; k++){ //for ever block above the new waitblock's Y position
							block.setChain(j, k, clearArray.get(i).getChain() + 1);
						}
						waitArray.add(new WaitSprite(AIRBLOCK ,j, top[j], (int) (WAITTIME * 1.5), clearArray.get(i).getChain() + 1));
					}
				}
				score += (int) (clearArray.get(i).getChain() *(SCORE_ON_CLEAR +  ((sprite.size() - 3) * 2)) * sprite.size());
				
				
				clearArray.remove(i);
			}
		}
	}
	
	public void rise(){
		boolean isRising = false;
		if(waitArray.size() == 0 && fallArray.size() == 0 && clearArray.size() == 0 && slideArray.size() == 0){
			isRising = true;
		}
		if((double) score / LEVEL_INTERVAL > level){
			level++;
			riseRate = BASE_RISE + (level * RISE_INTERVALS);
		}
		if(!riseButton){
			if(isRising){
				riseY -= riseRate * delta;
				if(riseY < -BLOCKSIZE){
					if(block.rise()){
						isRunning = false;
					}
					else{
						score += 5;
						riseY = 0;
						swapper.moveUp();
					}
				}
			}
		}
		else if(isRising){
			riseY -= BLOCKSIZE / 4;
			if(riseY < -BLOCKSIZE){
				if(block.rise()){
					riseY = -BLOCKSIZE;
					isRunning = false;
				}
				else{
					score += 10;
					riseY = 0;
					swapper.moveUp();
				}
				riseButton = false;
			}
		}
	}
}