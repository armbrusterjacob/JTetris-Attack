package puzzle;

public class ScoreSprite {
	final int BLOCKSIZE = 64;
	final int OFFSET = 32;
	final int WAITTIME = 60;
	final double SPEED = 0.25;
	private int x, num;
	private double y;
	private double waitTime;
	
	public ScoreSprite(int xPos, int yPos, int tNum){
		x = (xPos * BLOCKSIZE);
		y= (yPos * BLOCKSIZE) - OFFSET;
		num = tNum;
		waitTime = WAITTIME;
	}
	
	public void setX(int tX){x = tX;}
	public int getX(){return x;}
	
	public void setY(int tY){y = tY;}
	public double getY(){return y;}
	
	public int getNum(){return num;}
	
	public boolean Update(double delta){
		if(waitTime == 0){
			return true;
		}else{
			waitTime -= delta;
			if(waitTime < 0){
				waitTime = 0;
			}
			y -= SPEED;
		}
		return false;
	}
	
	
}
