package puzzle;


public class Sprite{
	private int x;
	private int y;
	private int waitTime;
	private int type;
	private boolean display;
	
	public Sprite(int tType, int tX, int tY, int tWaitTime){
		type = tType;
		x = tX;
		y= tY;
		waitTime = tWaitTime;
		display = true;
	}
	
	public void setX(int tX){x = tX;}
	public int getX(){return x;}
	
	public void setY(int tY){y = tY;}
	public int getY(){return y;}
	
	public void setType(int tType){type = tType;}
	public int getType(){return type;}
	
	public double getWaitTime(){return waitTime;}
	public void setWaitTime(int tWait){
		if(tWait > 0){
			waitTime = tWait;
		}
		else{
			waitTime = 0;
		}
	}
	public void decreaseTime(double delta){
		waitTime -= delta;
		if(waitTime < 0){
			waitTime = 0;
		}
	}
	
	public boolean getStatus(){return display;}
	public void toggleStatus(){
		if(display){
			display = false;
		}else{
			display = true;
		}
	}
}
