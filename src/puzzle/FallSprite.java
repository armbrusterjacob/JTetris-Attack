package puzzle;

import java.awt.*;

public class FallSprite {
	private double y;
	private int x,
				type,
				chain;
	
	public FallSprite(int tChain, int tType, int tX, double tY){
		type = tType;
		x = tX;
		y = tY;
		chain = tChain;
	}
	
	public void draw(Graphics2D g2){
		
	}
	
	public String toString(){return "direction = " + "Down" + "| x = " + x + " | y = " + y + " | type = " + type + " | chain = " + chain;}
	
	public void setX(int tX){x = tX;}
	public int getX(){return x;}
	
	public void setType(int tType){type = tType;}
	public int getType(){return type;}
	
	public void setY(double tY){y = tY;}
	public double getY(){return y;}

	public void setChain(int tChain){chain = tChain;}
	public int getChain(){return chain;}
}
