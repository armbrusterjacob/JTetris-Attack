package puzzle;

import java.awt.*;

public class SlideSprite {
	final boolean LEFT = false;
	final boolean RIGHT = true;
	final int BLOCK_SIZE = 64;
	private double x,
				   y;
	private int type,
				goalX,
				goalY,
				chain,
				airBlocks;
	private boolean direction;
	
	public SlideSprite(boolean tDirection, int tChain, int tType, double tX, double tY, int tAirBlocks){
		type = tType;
		x = tX;
		y = tY;
		direction = tDirection;
		chain = tChain;
		airBlocks = tAirBlocks;
		
		if(direction == LEFT){
			goalX = (int) x - 1;
		}else{
			goalX = (int) x + 1;
		}
		goalY = (int) tY;
	}
	
	public String toString(){
		if(direction == LEFT){
			return "direction = " + "Left " + "| x = " + x + " | y = " + y + " | type = " + type + " | goalX = " + goalX + " | goalY = " + goalY + " | chain = " + chain;
		}else{
			return "direction = " + "Right " + "| x = " + x + " | y = " + y + " | type = " + type + " | goalX = " + goalX + " | goalY = " + goalY + " | chain = " + chain;
		}
	}
	
	public void draw(Graphics2D g2){
		
	}
	
	public void setX(double tX){x = tX;}
	public double getX(){return x;}
	
	public void setType(int tType){type = tType;}
	public int getType(){return type;}
	
	public void setAir(int tAir){airBlocks = tAir;}
	public int getAir(){return airBlocks;}
	
	public void setY(double tY){y = tY;}
	public double getY(){return y;}
	
	public void setGoalX(int tGoalX){goalX = tGoalX;}
	public int getGoalX(){return goalX;}

	public void setGoalY(int tGoalY){goalY = tGoalY;}
	public int getGoalY(){return goalY;}
	
	public void setDirection(boolean bool){direction = bool;}
	public boolean getDirection(){return direction;}
	
}
