package puzzle;

import java.util.ArrayList;

public class ClearShape {
	final int AIRBLOCK = -1;
	final int ROWLENGTH = 6;
	final int COLUMNHEIGHT = 11;
	final int DELAY = 12;
	private int total = 0;
	private int numDisplayed = 0;
	private int chain;
	private int xStart;
	private int yStart;
	private ArrayList<Sprite> sprite = new ArrayList<Sprite>();
	
	ClearShape(int[][] shape, int tChain){
		xStart = ROWLENGTH;
		yStart = COLUMNHEIGHT - 1;
		
		for(int k = 0; k < COLUMNHEIGHT; k++){
			for(int i = 0; i < ROWLENGTH; i++){
				if(shape[i][k] != AIRBLOCK){
					if(i < xStart){
						xStart = i;
					}
					if(k < yStart){
						yStart = k;
					}
					total++;
					sprite.add(new Sprite(shape[i][k], i, k, (DELAY * (total + 2))));
				}
			}
		}
		
		numDisplayed = total;
		chain = tChain;
	}
	
	//if update returns true delete it
	public boolean update(double delta){
		for(int i = 0; i < sprite.size(); i++){
			if(sprite.get(i).getWaitTime() == 0){
				if(sprite.get(i).getStatus()){
					sprite.get(i).toggleStatus();
					numDisplayed--;
				}
			}else{
				sprite.get(i).decreaseTime(delta);
			}
		}
		
		if(numDisplayed == 0){
			return true;
		}
		return false;
	}
	
	public Sprite getSprite(int i){return sprite.get(i);}
	
	public int getNumOfBlocks(){
		return sprite.size();
	}
	
	public ArrayList<Sprite> getArrayList(){
		return sprite;
	}
	
	public void setChain(int tChain){chain = tChain;}
	public int getChain(){return chain;}
	
	public int getTotal(){return total;}
	
	public int getXStart(){return xStart;}
	public int getYStart(){return yStart;}
}
