package puzzle;

import java.awt.Graphics2D;
public class Swapper {
	final int BOARDWIDTH = 6;
	final int BOARDHEIGHT = 11;
	
	private int x;
	private int y;
	
	public Swapper(int tX, int tY){
		if(tX < 0){
			x = 0;
		}
		else if(tX > BOARDHEIGHT - 1){
			x = BOARDHEIGHT - 1;
		}
		else{
			x = tX;
		}
		
		if(tY < 0){
			y = 0;
		}
		else if(tY > BOARDHEIGHT - 1){
			y = BOARDHEIGHT - 1;
		}
		else{
			y = tY;
		}
	}
	
	public void draw(Graphics2D g2){
		
	}
	
	public void setPos(int tX, int tY){
		if(tX < 0){
			x = 0;
		}
		else if(tX > BOARDHEIGHT - 1){
			x = BOARDHEIGHT - 1;
		}
		else{
			x = tX;
		}
		
		if(tY < 0){
			y = 0;
		}
		else if(tY > BOARDHEIGHT - 1){
			y =  - 1;
		}
		else{
			y = tY;
		}
	}
	
	public void moveLeft(){
		int tX = x - 1;
		
		if(tX < 0){
			x = 0;
		}
		else{
			x = tX;
		}
	}
	
	public void moveRight(){
		int tX = x + 1;
		
		if(tX > BOARDWIDTH - 2){
			x = BOARDWIDTH - 2;
		}
		else{
			x = tX;
		}
	}
	
	public void moveUp(){
		int tY =  y - 1;
		
		if(tY < 0){
			y = 0;
		}
		else{
			y = tY;
		}
	}
	
	public void moveDown(){
		int tY = y + 1;
		
		if(tY > BOARDHEIGHT - 1){
			y = BOARDHEIGHT - 1;
		}
		else{
			y = tY;
		}
			
	}
	public int getX() {return x;}
	public int getY() {return y;}
	
	
}
