package puzzle;

import java.util.Random;

public class Block{
	final int ROWLENGTH = 6;
	final int COLUMNHEIGHT = 11;
	final int AIRBLOCK = -1;

	final String CURRENTTEST = "sets/set1.txt";
	private int ghostRow[] = new int [ROWLENGTH];
	private int block[][] = new int[ROWLENGTH][COLUMNHEIGHT];
	private int chain[][] = new int[ROWLENGTH][COLUMNHEIGHT];
	private int blockNum;
	Random rand = new Random();
	
	public Block(int tBlockNum){
		blockNum = tBlockNum;
		randomizeBlocks();
		generateGhostLine();
		
	}
	
	public void randomizeBlocks(){
		final int UPPERLIMIT = 6;
		final int LOWERLIMIT = 0;
		final int TOTALBLOCKS = 30;
		int max = UPPERLIMIT;
		int min = LOWERLIMIT;
		
		int blocksLeft = TOTALBLOCKS;
		int[] columnBlocks = new int[ROWLENGTH];
		
		for(int i = 0; i < ROWLENGTH; i++){
			for(int k = 0; k < COLUMNHEIGHT;  k++){
				block[i][k] = AIRBLOCK;
			}
		}
		
		for(int i = 0; i < ROWLENGTH; i++){
			columnBlocks[i] = rand.nextInt((max + 1) - min) + min;
			blocksLeft -= columnBlocks[i];
			
			if(i - 1 != ROWLENGTH && blocksLeft - ((ROWLENGTH - i - 2) * UPPERLIMIT) > LOWERLIMIT){
				min = blocksLeft - ((ROWLENGTH - i - 2) * UPPERLIMIT);
			}
			if(blocksLeft < UPPERLIMIT){
				max = blocksLeft;
			}
		}
		
		//shuffle the columns
		for(int i = 0; i < ROWLENGTH; i++){
			int j = rand.nextInt(ROWLENGTH);
			int temp = columnBlocks[i];
			columnBlocks[i] = columnBlocks[j];
			columnBlocks[j] = temp;
		}
		
		boolean isMatching = true;
		for(int i = 0; i < ROWLENGTH; i++){
			for(int k = COLUMNHEIGHT - columnBlocks[i]; k < COLUMNHEIGHT; k++){
				while(isMatching){
					block[i][k] = rand.nextInt(blockNum);
					if(!(block[i][k-1] == block[i][k-2] && block[i][k] == block[i][k-1]) &&
					   !(i > 1 && block[i-1][k] == block[i-2][k] && block[i][k] == block[i-1][k]) &&
					   !(i < ROWLENGTH - 2 && block[i+1][k] == block[i+2][k] && block[i][k] == block[i+1][k]) &&
					   !(k < COLUMNHEIGHT - 2 && block[i][k+1] == block[i][k+2] && block[i][k] == block[i][k+1]) &&
					   !(k < COLUMNHEIGHT - 1 && block[i][k-1] == block[i][k+1] && block[i][k] == block[i][k-1]) &&
					   !(i > 0 && i < ROWLENGTH - 1 && block[i-1][k] == block[i+1][k] && block[i][k] == block[i-1][k])){
					       	
						isMatching = false;
					}
				}
				isMatching = true;
			}
		}
	}
	
	public void generateGhostLine(){
		boolean isMatching = true;
		
		for(int i = 0; i < ROWLENGTH; i++){
			ghostRow[i] = AIRBLOCK;
		}
		
		for(int i = 0; i < ROWLENGTH; i++){
			while(isMatching){
				ghostRow[i] = rand.nextInt(blockNum);
				if(!(ghostRow[i] == block[i][COLUMNHEIGHT - 1] && ghostRow[i] == block[i][COLUMNHEIGHT - 2]) &&
				   !(i > 1 && ghostRow[i] == ghostRow[i - 1] && ghostRow[i] == ghostRow[i - 2]) &&
				   !(i < ROWLENGTH - 2 && ghostRow[i] == ghostRow[i + 1] && ghostRow[i] == ghostRow[i + 2]) &&
				   !(i > 0 && i < ROWLENGTH - 1 && ghostRow[i] == ghostRow[i -1] && ghostRow[i] == ghostRow[i + 1])){
					
					isMatching = false;
				}
			}
			isMatching = true;
		}
	}
	
	public boolean rise(){
		//returns true if a block in the top row exists while rising
		for(int i = 0; i < ROWLENGTH; i++){
			if(block[i][0] != AIRBLOCK){
				return true;
			}
		}
		
		for(int i = 0; i < ROWLENGTH; i++){
			for(int k = 0; k < COLUMNHEIGHT - 1; k++){
				block[i][k] = block[i][k + 1];
				chain[i][k] = chain[i][k + 1];
			}
		}
		
		for(int i = 0; i < ROWLENGTH; i++){
			block[i][COLUMNHEIGHT - 1] = ghostRow[i];
			chain[i][COLUMNHEIGHT - 1] = 0;
		}
		generateGhostLine();
		
		return false;
	}
	
	public void setType(int x, int y, int tType) {block[x][y] = tType;}
	public int getType(int x, int y) {return block[x][y];}
	
	public void setChain(int x, int y, int tChain) {chain[x][y] = tChain;}
	public int getChain(int x, int y) {return chain[x][y];}
	
	public int[] getGhostArray(){return ghostRow;}
	
	
}
