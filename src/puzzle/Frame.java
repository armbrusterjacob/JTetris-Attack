package puzzle;
import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;



public class Frame extends JPanel{
	public static JPanel panel = new JPanel();
	public static void main(String[] args)
	{
		JFrame frame = new JFrame("Puzzle Clone");
		final int FRAMEWIDTH = 700;
		final int FRAMEHEIGHT = 900;
		final int CHARACTER = 2;
		final int BLOCKNUM = 5;
		frame.getContentPane().setPreferredSize(new Dimension(FRAMEWIDTH,FRAMEHEIGHT));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Game game = new Game(FRAMEWIDTH, FRAMEHEIGHT, 20, 20, CHARACTER, BLOCKNUM);
		frame.add(game);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		new Thread(new Runnable(){
			public void run(){
				game.gameLoop();
			}
		}).start();
	}
}
	

