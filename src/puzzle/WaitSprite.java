package puzzle;

import java.awt.*;

public class WaitSprite {
	private int x, y, type, chain;
	private double waitTime;
	
	public WaitSprite(int tType,int tX, int tY, int tWait, int tChain){
		type = tType;
		x = tX;
		y = tY;
		waitTime = tWait;
		chain = tChain;
	}
	
	public void setType(int tType){type = tType;}
	public int getType(){return type;}
	
	public void setX(int tX){x = tX;}
	public int getX(){return x;}
	
	public void setY(int tY){y = tY;}
	public int getY(){return y;}
	
	public void setChain(int tChain){chain = tChain;}
	public int getChain(){return chain;}
	
	public double getWaitTime(){return waitTime;}
	public void setWaitTime(int tWait){
		if(tWait > 0){
			waitTime = tWait;
		}
		else{
			waitTime = 0;
		}
	}
	
	public void draw(Graphics2D g2){
		
	}
	
	public void decreaseTime(double delta){
		waitTime -= delta;
		if(waitTime < 0){
			waitTime = 0;
		}
	}
}
