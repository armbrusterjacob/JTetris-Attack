package puzzle;

public class LossSprite {
	private int x, y, leftBoarder, rightBoarder;
	private boolean goingRight;
	
	public LossSprite(int xPos, int yPos, int rBoarder){
		x = xPos;
		y = yPos;
		leftBoarder = x;
		rightBoarder = xPos + rBoarder;
	}
	
	public void setX(int tX){x = tX;}
	public int getX(){return x;}
	
	public void setY(int tY){y = tY;}
	public int getY(){return y;}
	
	public void update(){
		if(goingRight){
			x++;
		}else{
			x--;
		}
		
		if(x > rightBoarder){
			goingRight = false;
		}else if(x < leftBoarder){
			goingRight = true;
		}
	}
}
